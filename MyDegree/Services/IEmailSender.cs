﻿using System.Threading.Tasks;

namespace MyDegree.Services
{
    public interface IEmailSender
    {
        Task SendEmailAsync(string email, string subject, string message);
    }
}
