﻿using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.TestHost;
using Xunit;

namespace MyDegree.IntegrationTests
{
    public class DefaultRequestShould
    {
        private readonly TestServer _server;
        private readonly HttpClient _client;
        public DefaultRequestShould()
        {
            // Arrange
            _server = new TestServer(new WebHostBuilder()
                .UseStartup<Startup>());
            _client = _server.CreateClient();
        }

        [Fact]
        public async Task ReturnView()
        {
            // Act
            var response =  await _client.GetAsync("/");
     
            // Assert
            var viewResult = Assert.IsType<ViewResult>(response);
        }
    }
}